# Arabic translation for camera-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the camera-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: camera-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-09-29 12:12+0000\n"
"PO-Revision-Date: 2023-07-17 18:05+0000\n"
"Last-Translator: abidin toumi <abidin24@disroot.org>\n"
"Language-Team: Arabic <https://hosted.weblate.org/projects/lomiri/"
"lomiri-camera-app/ar/>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Weblate 5.0-dev\n"
"X-Launchpad-Export-Date: 2016-12-01 04:53+0000\n"

#: lomiri-camera-app.desktop.in:4 lomiri-camera-app.desktop.in:6
#: Information.qml:106
msgid "Camera"
msgstr "الكاميرا"

#: lomiri-camera-app.desktop.in:5
msgid "Camera application"
msgstr "تطبيق الكاميرا"

#: lomiri-camera-app.desktop.in:7
msgid "Photos;Videos;Capture;Shoot;Snapshot;Record"
msgstr "صور;فيديو;إلتقاط;التقاط;لقطة;تسجيل"

#: lomiri-camera-app.desktop.in:10
msgid "/usr/share/lomiri-camera-app/lomiri-camera-app.png"
msgstr "/usr/share/lomiri-camera-app/lomiri-camera-app.png"

#: AdvancedOptions.qml:23 PhotogridView.qml:69 SlideshowView.qml:77
msgid "Settings"
msgstr "الإعدادات"

#: AdvancedOptions.qml:27
msgid "Close"
msgstr "أغلق"

#: AdvancedOptions.qml:35 Information.qml:20 PhotogridView.qml:78
#: SlideshowView.qml:86
msgid "About"
msgstr "حول"

#: AdvancedOptions.qml:66
msgid "Add date stamp on captured images"
msgstr "أضف ختم التاريخ على الصور الملتقطة"

#. TRANSLATORS: this refers to the opacity  of date stamp added to captured images
#: AdvancedOptions.qml:89
msgid "Format"
msgstr "هيّء"

#: AdvancedOptions.qml:142
msgid "Date formatting keywords"
msgstr "الكلمات المفتاحية لنسق التاريخ"

#: AdvancedOptions.qml:147
msgid "the day as number without a leading zero (1 to 31)"
msgstr "الأيام كأرقام بدون الأصفار البادئة (من 1 حتى 31)"

#: AdvancedOptions.qml:148
msgid "the day as number with a leading zero (01 to 31)"
msgstr "الأيام كأرقام بالأصفار البادئة (من 01 حتى 31)"

#: AdvancedOptions.qml:149
msgid "the abbreviated localized day name (e.g. 'Mon' to 'Sun')."
msgstr "أسماء الأيام المختصرة(مثل 'أحد' إلى 'إثن'."

#: AdvancedOptions.qml:150
msgid "the long localized day name (e.g. 'Monday' to 'Sunday')."
msgstr "أسماء الأيام الكاملة (مثل 'السبت' حتى 'الجمعة')."

#: AdvancedOptions.qml:151
msgid "the month as number without a leading zero (1 to 12)"
msgstr "الأشهر كأرقام بدون الأصفار البادئة (من 1 حتى 12)"

#: AdvancedOptions.qml:152
msgid "the month as number with a leading zero (01 to 12)"
msgstr "الأشهر كأرقام بالأصفار البادئة (من 01 حتى 12)"

#: AdvancedOptions.qml:153
msgid "the abbreviated localized month name (e.g. 'Jan' to 'Dec')."
msgstr "اختصارات أسماء الأشهر (مثل 'جان' حتى 'ديس')."

#: AdvancedOptions.qml:154
msgid "the long localized month name (e.g. 'January' to 'December')."
msgstr "أسماء الأشهر كاملة (مثل 'جانفي' حتى 'ديسمبر')."

#: AdvancedOptions.qml:155
msgid "the year as two digit number (00 to 99)"
msgstr "السنة كرقمين (00 حتى 99)"

#: AdvancedOptions.qml:156
msgid ""
"the year as four digit number. If the year is negative, a minus sign is "
"prepended in addition."
msgstr "السنة كأربع أرقام. إذا كانت السنة سالبة، تضاف إشارة سالب."

#: AdvancedOptions.qml:157
msgid "the hour without a leading zero (0 to 23 or 1 to 12 if AM/PM display)"
msgstr "الساعة بدون صفر(0 إلى 23 أو واحد إلى 12 في عرض الصباح أو المساء)"

#: AdvancedOptions.qml:158
msgid "the hour with a leading zero (00 to 23 or 01 to 12 if AM/PM display)"
msgstr "الساعة بدون صفر(00 إلى 23 أو 01 إلى 12 في عرض الصباح أو المساء)"

#: AdvancedOptions.qml:159
msgid "the hour without a leading zero (0 to 23, even with AM/PM display)"
msgstr "الساعة بدون صفر(0 إلى 23، حتى مع عرض الصباح أو المساء)"

#: AdvancedOptions.qml:160
msgid "the hour with a leading zero (00 to 23, even with AM/PM display)"
msgstr "الساعة مع صفر(00 إلى 23، حتى مع عرض الصباح أو المساء)"

#: AdvancedOptions.qml:161
msgid "the minute without a leading zero (0 to 59)"
msgstr "الدقائق بدون الأصفار البادئة (من 0 حتى 59)"

#: AdvancedOptions.qml:162
msgid "the minute with a leading zero (00 to 59)"
msgstr "الدقائق بالأصفار البادئة (من 00 حتى 59)"

#: AdvancedOptions.qml:163
msgid "the second without a leading zero (0 to 59)"
msgstr "الثواني بدون الأصفار البادئة (من 0 حتى 59)"

#: AdvancedOptions.qml:164
msgid "the second with a leading zero (00 to 59)"
msgstr "الثواني بالأصفار البادئة (من 00 حتى 59)"

#: AdvancedOptions.qml:165
msgid "the milliseconds without leading zeroes (0 to 999)"
msgstr "الملي ثانية بدون الأصفار البادئة (0 حتى 999)"

#: AdvancedOptions.qml:166
msgid "the milliseconds with leading zeroes (000 to 999)"
msgstr "الملي ثانية بالأصفار البادئة (000 حتى 999)"

#: AdvancedOptions.qml:167
msgid "use AM/PM display. AP will be replaced by either 'AM' or 'PM'."
msgstr "استخدم نسق 12 ساعة. ستتم إضافة 'ص' أو 'م' حسب الوقت."

#: AdvancedOptions.qml:168
msgid "use am/pm display. ap will be replaced by either 'am' or 'pm'."
msgstr "استخدم نسق 12 ساعة. ستتم إضافة 'ص' أو 'م' حسب الوقت."

#: AdvancedOptions.qml:169
msgid "the timezone (for example 'CEST')"
msgstr "المنطقة الزمنية (مثل 'CEST')"

#: AdvancedOptions.qml:178
msgid "Add to Format"
msgstr "أضف الى النسق"

#. TRANSLATORS: this refers to the color of date stamp added to captured images
#: AdvancedOptions.qml:209
msgid "Color"
msgstr "اللون"

#. TRANSLATORS: this refers to the alignment of date stamp within captured images (bottom left, top right,etc..)
#: AdvancedOptions.qml:278
msgid "Alignment"
msgstr "المحاذاة"

#. TRANSLATORS: this refers to the opacity  of date stamp added to captured images
#: AdvancedOptions.qml:323
msgid "Opacity"
msgstr "العتمة"

#: AdvancedOptions.qml:346
msgid "Blurred Overlay"
msgstr ""

#: AdvancedOptions.qml:367
msgid "Only Blur Preview overlay"
msgstr ""

#: DeleteDialog.qml:24
msgid "Delete media?"
msgstr "أتريد حذف الملف؟"

#: DeleteDialog.qml:34 PhotogridView.qml:56 SlideshowView.qml:69
msgid "Delete"
msgstr "احذف"

#: DeleteDialog.qml:40 ViewFinderOverlay.qml:1197 ViewFinderOverlay.qml:1211
#: ViewFinderOverlay.qml:1259 ViewFinderView.qml:404
msgid "Cancel"
msgstr "إلغاء"

#: GalleryView.qml:270
msgid "No media available."
msgstr "لا تتوفر وسائط."

#: GalleryView.qml:305
msgid "Scanning for content..."
msgstr "يبحث عن محتوى..."

#: GalleryViewHeader.qml:87 SlideshowView.qml:46
msgid "Select"
msgstr "حدد"

#: GalleryViewHeader.qml:88
msgid "Edit Photo"
msgstr "حرر صورة"

#: GalleryViewHeader.qml:88
msgid "Photo Roll"
msgstr "لف الصورة"

#: Information.qml:25
msgid "Back"
msgstr "للخلف"

#: Information.qml:76
msgid "Get the source"
msgstr "احصل على المصدر"

#: Information.qml:77
msgid "Report issues"
msgstr "أبلغ عن علل"

#: Information.qml:78
msgid "Help translate"
msgstr "ساعد في الترجمة"

#: MediaInfoPopover.qml:14
#, qt-format
msgid "Width : %1"
msgstr "العرض: %1"

#: MediaInfoPopover.qml:15
#, qt-format
msgid "Height : %1"
msgstr "الارتفاع: %1"

#: MediaInfoPopover.qml:16
#, qt-format
msgid "Date : %1"
msgstr "تاريخ: %1"

#: MediaInfoPopover.qml:17
#, qt-format
msgid "Camera Model : %1"
msgstr "طراز الكاميرا: %1"

#: MediaInfoPopover.qml:18
#, qt-format
msgid "Copyright : %1"
msgstr "حقوق النشر: %1"

#: MediaInfoPopover.qml:19
#, qt-format
msgid "Exposure Time : %1"
msgstr ""

#: MediaInfoPopover.qml:20
#, fuzzy, qt-format
#| msgid "Name : %1"
msgid "F. Number : %1"
msgstr "الاسم: %1"

#: MediaInfoPopover.qml:21
#, qt-format
msgid "Sub-File type : %1"
msgstr "نوع الملف الفرعي: %1"

#: MediaInfoPopover.qml:43
msgid "Media Information"
msgstr "معلومات الوسائط"

#: MediaInfoPopover.qml:48
#, qt-format
msgid "Name : %1"
msgstr "الاسم: %1"

#: MediaInfoPopover.qml:51
#, qt-format
msgid "Type : %1"
msgstr "النوع: %1"

#: MediaInfoPopover.qml:83
#, fuzzy, qt-format
#| msgid "Width : %1"
msgid "With Flash : %1"
msgstr "العرض: %1"

#: MediaInfoPopover.qml:83
msgid "Yes"
msgstr "نعم"

#: MediaInfoPopover.qml:83
msgid "No"
msgstr "لا"

#: NoSpaceHint.qml:33
msgid "No space left on device, free up space to continue."
msgstr "لا توجد مساحة كافية في الجهاز، رجاء حرر مساحة للمتابعة."

#: PhotoRollHint.qml:69
msgid "Swipe left for photo roll"
msgstr "اسحب لليسار للف الصورة"

#: PhotogridView.qml:42 SlideshowView.qml:54
msgid "Share"
msgstr "شارك"

#: SlideshowView.qml:62
msgid "Image Info"
msgstr "معلومات الصورة"

#: SlideshowView.qml:98
msgid "Edit"
msgstr "حرر"

#: SlideshowView.qml:477
msgid "Back to Photo roll"
msgstr "العودة إلى لف الصورة"

#: UnableShareDialog.qml:25
msgid "Unable to share"
msgstr "تعذرت المشاركة"

#: UnableShareDialog.qml:26
msgid "Unable to share photos and videos at the same time"
msgstr "تتعذر مشاركة الصور ومقاطع الفيديو في نفس الوقت"

#: UnableShareDialog.qml:30
msgid "Ok"
msgstr "موافق"

#: ViewFinderOverlay.qml:455 ViewFinderOverlay.qml:478
#: ViewFinderOverlay.qml:506 ViewFinderOverlay.qml:529
#: ViewFinderOverlay.qml:614 ViewFinderOverlay.qml:681
msgid "On"
msgstr "مفتوح"

#: ViewFinderOverlay.qml:460 ViewFinderOverlay.qml:488
#: ViewFinderOverlay.qml:511 ViewFinderOverlay.qml:534
#: ViewFinderOverlay.qml:553 ViewFinderOverlay.qml:619
#: ViewFinderOverlay.qml:691
msgid "Off"
msgstr "أطفئ"

#: ViewFinderOverlay.qml:483
msgid "Auto"
msgstr "آلي"

#: ViewFinderOverlay.qml:520
msgid "HDR"
msgstr "HDR"

#: ViewFinderOverlay.qml:558
msgid "5 seconds"
msgstr "5 ثوان"

#: ViewFinderOverlay.qml:563
msgid "15 seconds"
msgstr "15 ثانية"

#: ViewFinderOverlay.qml:581
msgid "Fine Quality"
msgstr "جودة جيدة"

#: ViewFinderOverlay.qml:586
msgid "High Quality"
msgstr "جودة عالية"

#: ViewFinderOverlay.qml:591
msgid "Normal Quality"
msgstr "جودة عادية"

#: ViewFinderOverlay.qml:596
msgid "Basic Quality"
msgstr "جودة أساسية"

#. TRANSLATORS: this will be displayed on an small button so for it to fit it should be less then 3 characters long.
#: ViewFinderOverlay.qml:629
msgid "SD"
msgstr "بطاقة الذاكرة"

#: ViewFinderOverlay.qml:637
msgid "Save to SD Card"
msgstr "حفظ إلى بطاقة الذاكرة"

#: ViewFinderOverlay.qml:642
msgid "Save internally"
msgstr "احفظ داخليا"

#: ViewFinderOverlay.qml:686
msgid "Vibrate"
msgstr "اهتز"

#: ViewFinderOverlay.qml:1194
msgid "Low storage space"
msgstr "مساحة تخزين منخفضة"

#: ViewFinderOverlay.qml:1195
msgid ""
"You are running out of storage space. To continue without interruptions, "
"free up storage space now."
msgstr ""
"مساحة التخزين على وشك النفاذ. للمتابعة دون انقطاع، رجاء حرر بعض المساحة الآن."

#: ViewFinderOverlay.qml:1208
msgid "External storage not writeable"
msgstr "التخزين الخارجي غير قابل للكتابة"

#: ViewFinderOverlay.qml:1209
msgid ""
"It does not seem possible to write to your external storage media. Trying to "
"eject and insert it again might solve the issue, or you might need to format "
"it."
msgstr ""
"يبدو انه لا يمكن الكتابة إلى وسائط التخزين الخارجية. قد يحل إخراجها وإعادة "
"إدخالها المشكلة، أو قد تحتاج إلى تهيئته."

#: ViewFinderOverlay.qml:1247
msgid "Cannot access camera"
msgstr "لا يمكن الوصول للكاميرا"

#: ViewFinderOverlay.qml:1248
msgid ""
"Camera app doesn't have permission to access the camera hardware or another "
"error occurred.\n"
"\n"
"If granting permission does not resolve this problem, reboot your device."
msgstr ""
"تطبيق الكاميرا ليس لديه الإذن للنفاذ إلى جهاز الكاميرا أو أن خطأ آخر حدث.\n"
"\n"
"إذا لم يحل منح الأذون للتطبيق المشكلة، رجاء أعد تشغل الهاتف."

#: ViewFinderOverlay.qml:1250
msgid "Edit Permissions"
msgstr "حرر الأذون"

#: ViewFinderView.qml:395
msgid "Capture failed"
msgstr "فشل الالتقاط"

#: ViewFinderView.qml:400
msgid ""
"Replacing your external media, formatting it, or restarting the device might "
"fix the problem."
msgstr ""
"قد يحل استبدال الوسائط الخارجية أو تهيئتها أو إعادة تشغيل الجهاز المشكلة."

#: ViewFinderView.qml:401
msgid "Restarting your device might fix the problem."
msgstr "قد يحل إعادة تشغيل الجهاز المشكلة."

#: camera-app.qml:65
msgid "Flash"
msgstr "الوميض"

#: camera-app.qml:66
msgid "Light;Dark"
msgstr "الإضاءة;الظلمة"

#: camera-app.qml:69
msgid "Flip Camera"
msgstr "اقلب الكاميرا"

#: camera-app.qml:70
msgid "Front Facing;Back Facing"
msgstr "المواجهة الأمامية;المواجهة الخلفية"

#: camera-app.qml:73
#, fuzzy
msgid "Shutter"
msgstr "إلتقاط"

#: camera-app.qml:74
msgid "Take a Photo;Snap;Record"
msgstr "إلتقط صورة;سجل فيديو"

#: camera-app.qml:77
msgid "Mode"
msgstr "الوضع"

#: camera-app.qml:78
msgid "Stills;Video"
msgstr "صور ثابتة;فيديو"

#: camera-app.qml:82
msgid "White Balance"
msgstr "موازنة اللون الأبيض"

#: camera-app.qml:83
msgid "Lighting Condition;Day;Cloudy;Inside"
msgstr "حالة الإضاءة;نهار;قاتم;في الداخل"

#: camera-app.qml:387
#, qt-format
msgid "<b>%1</b> photos taken today"
msgstr "صور <b>%1</b> الملتقطة اليوم"

#: camera-app.qml:388
msgid "No photos taken today"
msgstr "لم تلتقط صور اليوم"

#: camera-app.qml:398
#, qt-format
msgid "<b>%1</b> videos recorded today"
msgstr "فيديوهات <b>%1</b> المسجلة اليوم"

#: camera-app.qml:399
msgid "No videos recorded today"
msgstr "لم تسجل فيديوهات اليوم"

#~ msgid "Gallery"
#~ msgstr "المعرض"

#~ msgid "Version %1"
#~ msgstr "النسخة 1"

#~ msgid "Advanced Options"
#~ msgstr "خيارات متقدمة"
